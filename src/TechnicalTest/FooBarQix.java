package TechnicalTest;

import java.util.HashMap;
import java.util.Map;

public class FooBarQix {

    private static HashMap<Character, String> keywords ;
    private static HashMap<Character, String> keywords2 ;

    static {
        keywords= new HashMap<> () ;
        keywords.put('3',"Foo") ;
        keywords.put('5',"Bar") ;
        keywords.put('7',"Qix") ;

        keywords2=new HashMap<>(keywords);
        keywords2.put('0',"*") ;
    }

    private StringBuilder  checkDivisible(String input){
        StringBuilder printResult = new StringBuilder();
        int result = Integer.parseInt(input) ;

        for (Character key : keywords.keySet()) {
            if (result % Character.getNumericValue(key) == 0) {
                printResult.append(keywords.get(key));
            }
        }
     return  printResult ;
    }


    // this function checks if the string contains the words (Foo or Bar or Qix)
    private boolean containsString(StringBuilder printResult) {
        boolean isContain = false;
        for (Map.Entry<Character, String> entry : keywords.entrySet()) {
            if (printResult.toString().contains(entry.getValue())) {
                isContain = true;
            }
        }

        return isContain;
    }

      private String checkContainsSpecificChar(String input,StringBuilder printResult){
        for (char c : input.toCharArray()) {
            if (keywords2.containsKey(c)) {
                printResult.append(keywords2.get(c));
            }
        }

        if (printResult.equals("")) return input  ;
        else if ( containsString(printResult))   return printResult.toString() ;
        else { return input.replace("0","*") ; }

    }


    protected  String compute(String input){
        if (Integer.parseInt(input)<0) return "Please set positive values";
        StringBuilder resultat = checkDivisible( input);
        return   checkContainsSpecificChar(input, resultat) ;
    }



}

